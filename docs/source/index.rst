.. Microfinance documentation master file, created by
   sphinx-quickstart on Wed Aug 25 19:56:33 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Microfinance dAppDocumentation
========================================

.. toctree::
   :maxdepth: 2
   :caption: Introduction

   introduction/applicationOverview
   introduction/functionalities

.. toctree::
   :maxdepth: 2
   :caption: Installation Guide

   installation/guide
   installation/checkout
   installation/structure
   installation/deploySmartContract
   installation/metamask
   installation/bankServer
   installation/bankApp
   installation/connectReactMetaMask
